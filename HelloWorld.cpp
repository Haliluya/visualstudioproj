﻿#include <iostream>
//#include "Header.h"


class Animal
{
public:
    virtual void Voice()
    {
        std::cout << "Guess who?";
    }
};

class Dog : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Bark!";
    }
};

class Cat : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Meow!";
    }
};

class Cow : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Moo!";
    }
};
using namespace std;
int main()
{
    Animal* objects[3];
    objects[0] = new Dog;
    objects[1] = new Cat;
    objects[2] = new Cow;
    for (int i = 0; i < 3; i++)
    {
         objects[i]->Voice();
    }
    

}
